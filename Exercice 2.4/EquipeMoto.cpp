#include "EquipeMoto.h"
#include <iostream>

EquipeMoto::EquipeMoto(std::string nom, std::string nomManager) : nom(nom) {
    manager = new Personne(nomManager);
    for (int i = 0; i < 3; ++i) {
        lesPilotes[i] = nullptr;
    }
}

EquipeMoto::~EquipeMoto() {
    delete manager;
    for (int i = 0; i < 3; ++i) {
        delete lesPilotes[i];
    }
}

std::string EquipeMoto::getNom() const {
    return nom;
}

void EquipeMoto::setNom(std::string nom) {
    this->nom = nom;
}

Personne* EquipeMoto::getManager() const {
    return manager;
}

void EquipeMoto::AddPilote(unsigned int rang, Personne* pilote) {
    if (rang < 3) {
        lesPilotes[rang] = pilote;
    }
}

Personne** EquipeMoto::GetPilotes() {
    return lesPilotes;
}
