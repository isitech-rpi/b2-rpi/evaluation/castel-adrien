#ifndef EQUIPEMOTO_H
#define EQUIPEMOTO_H

#include <string>
#include "Personne.h"

class EquipeMoto {
private:
    std::string nom;
    Personne* manager;
    Personne* lesPilotes[3];
public:
    EquipeMoto(std::string nom, std::string nomManager);
    ~EquipeMoto();

    std::string getNom() const;
    void setNom(std::string nom);

    Personne* getManager() const;

    void AddPilote(unsigned int rang, Personne* pilote);
    Personne** GetPilotes();
};

#endif
