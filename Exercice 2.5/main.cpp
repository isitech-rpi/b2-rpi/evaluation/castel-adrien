#include <iostream>
#include "Personne.h"
#include "EquipeMoto.h"


void AffichePersonne(const Personne& p) {
    std::cout << "Personne(" << &p << "): " << p.getNom() << std::endl;
}


Personne* CreerPersonne() {
    std::string nom;
    std::cout << "Entrez le nom de la personne: ";
    std::cin >> nom;
    return new Personne(nom);
}


void AfficheEquipeMoto(EquipeMoto* equipe) {
    std::cout << "Equipe: " << equipe->getNom() << std::endl;
    std::cout << "Manager: " << equipe->getManager()->getNom() << std::endl;

    Personne** pilotes = equipe->GetPilotes();
    for (int i = 0; i < 3; ++i) {
        if (pilotes[i]) {
            std::cout << "Pilote[" << i << "]: " << pilotes[i]->getNom() << std::endl;
        }
        else {
            std::cout << "Pas de pilote[" << i << "]" << std::endl;
        }
    }
}

int main() {

    Personne pilote_1("Fabio");
    std::cout << "Adresse de pilote_1: " << &pilote_1 << std::endl;
    std::cout << "Nom de pilote_1: " << pilote_1.getNom() << std::endl;


    Personne* pilote_2 = CreerPersonne();
    AffichePersonne(pilote_1);
    AffichePersonne(*pilote_2);


    EquipeMoto* equipe_1 = new EquipeMoto("YMF", "Jarvis");
    std::cout << "Adresse de equipe_1: " << equipe_1 << std::endl;
    std::cout << "Nom de equipe_1: " << equipe_1->getNom() << std::endl;
    std::cout << "Nom du manager de equipe_1: " << equipe_1->getManager()->getNom() << std::endl;


    equipe_1->AddPilote(0, &pilote_1);
    equipe_1->AddPilote(1, pilote_2);
    AfficheEquipeMoto(equipe_1);


    Personne* pil_pramac_0 = CreerPersonne();
    Personne* pil_pramac_2 = CreerPersonne();

    EquipeMoto pramac = *equipe_1;
    pramac.setNom("Pramac");
    pramac.getManager()->setNom("Campignoti");
    pramac.AddPilote(0, pil_pramac_0);
    pramac.AddPilote(2, pil_pramac_2);

    AfficheEquipeMoto(equipe_1);
    AfficheEquipeMoto(&pramac);


    delete pilote_2;
    delete pil_pramac_0;
    delete pil_pramac_2;
    delete equipe_1;

    return 0;
}
