#include <iostream>
#include "Personne.h"


void AffichePersonne(const Personne& p) {
    std::cout << "Personne(" << &p << "): " << p.getNom() << std::endl;
}


Personne* CreerPersonne() {
    std::string nom;
    std::cout << "Entrez le nom de la personne: ";
    std::cin >> nom;
    return new Personne(nom);
}

int main() {

    Personne pilote_1("Fabio");

    std::cout << "Adresse de pilote_1: " << &pilote_1 << std::endl;
    std::cout << "Nom de pilote_1: " << pilote_1.getNom() << std::endl;


    Personne* pilote_2 = CreerPersonne();

    AffichePersonne(pilote_1);
    AffichePersonne(*pilote_2);


    delete pilote_2;

    return 0;
}
