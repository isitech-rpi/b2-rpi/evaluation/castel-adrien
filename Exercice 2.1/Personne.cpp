#include "Personne.h"

Personne::Personne(std::string nom) : nom(nom) {}

std::string Personne::getNom() const {
    return nom;
}

void Personne::setNom(std::string nom) {
    this->nom = nom;
}
