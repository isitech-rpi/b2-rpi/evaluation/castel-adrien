#ifndef PERSONNE_H
#define PERSONNE_H

#include <string>

class Personne {
private:
    std::string nom;
public:
    Personne(std::string nom);
    std::string getNom() const;
    void setNom(std::string nom);
};

#endif 