#include <iostream>
#include "Personne.h"

int main() {
    Personne pilote_1("Fabio");

    std::cout << "Adresse de pilote_1: " << &pilote_1 << std::endl;
    std::cout << "Nom de pilote_1: " << pilote_1.getNom() << std::endl;

    return 0;
}
