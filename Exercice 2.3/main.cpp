#include <iostream>
#include "Personne.h"
#include "EquipeMoto.h"


void AffichePersonne(const Personne& p) {
    std::cout << "Personne(" << &p << "): " << p.getNom() << std::endl;
}


Personne* CreerPersonne() {
    std::string nom;
    std::cout << "Entrez le nom de la personne: ";
    std::cin >> nom;
    return new Personne(nom);
}

int main() {

    Personne pilote_1("Fabio");
    std::cout << "Adresse de pilote_1: " << &pilote_1 << std::endl;
    std::cout << "Nom de pilote_1: " << pilote_1.getNom() << std::endl;


    Personne* pilote_2 = CreerPersonne();
    AffichePersonne(pilote_1);
    AffichePersonne(*pilote_2);

 
    EquipeMoto* equipe_1 = new EquipeMoto("YMF", "Jarvis");

    std::cout << "Adresse de equipe_1: " << equipe_1 << std::endl;
    std::cout << "Nom de equipe_1: " << equipe_1->getNom() << std::endl;
    std::cout << "Nom du manager de equipe_1: " << equipe_1->getManager()->getNom() << std::endl;


    delete pilote_2;
    delete equipe_1;

    return 0;
}
