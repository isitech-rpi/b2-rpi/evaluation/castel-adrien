#include "EquipeMoto.h"

EquipeMoto::EquipeMoto(std::string nom, std::string nomManager) : nom(nom) {
    manager = new Personne(nomManager);
}

EquipeMoto::~EquipeMoto() {
    delete manager;
}

std::string EquipeMoto::getNom() const {
    return nom;
}

void EquipeMoto::setNom(std::string nom) {
    this->nom = nom;
}

Personne* EquipeMoto::getManager() const {
    return manager;
}
